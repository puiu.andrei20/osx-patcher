#include "macros.h"

/*
Adds stubs for functions that libGFXShared.dylib added in 10.8
*/

make_return_zero(gfxGetGFXBufferForGFXTexture) // used in opencl but ONLY for gpu not cpu

// will be reexported from 10.8 lib
// gfxFindIOFramebufferWithOpenGLDisplayMask

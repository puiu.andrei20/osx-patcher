#include <dlfcn.h>
#include <stdio.h>
int main(int argc, char **argv) {
  void* handle = dlopen(argv[1], RTLD_NOW);
  if (handle == NULL) {
    fprintf(stderr, "Could not open dylib: %s\n", dlerror());
    return 1;
  }
  if (dlclose(handle) != 0) {
    fprintf(stderr, "Could not close dylib: %s\n", dlerror());
    return 1;
  }
    printf("OK\n");
  return 0;
}

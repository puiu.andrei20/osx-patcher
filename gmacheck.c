/* 
by Ethan Nelson-Moore (ParrotGeek Software)
Based on code from https://github.com/tensorflow/tensorflow/blob/4692525ffaa79982f094979f564b0b4942732ff5/tensorflow/stream_executor/cuda/cuda_diagnostics.cc, which is:

Copyright 2015 The TensorFlow Authors. All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

#include <IOKit/kext/KextManager.h>

int main(void) {
    CFStringRef kext_ids[1];
    kext_ids[0] = CFSTR("com.apple.driver.AppleIntelGMA950");
    CFArrayRef kext_id_query = CFArrayCreate(NULL, (const void **)kext_ids, 1,
                                             &kCFTypeArrayCallBacks);
    CFDictionaryRef kext_infos =
    KextManagerCopyLoadedKextInfo(kext_id_query, NULL);
    CFRelease(kext_id_query);

    CFDictionaryRef driver_info = NULL;
    if (CFDictionaryGetValueIfPresent(kext_infos, kext_ids[0],
                                      (const void **)&driver_info)) {
        bool started = CFBooleanGetValue((CFBooleanRef)CFDictionaryGetValue(
                                                                            driver_info, CFSTR("OSBundleStarted")));
        if (started) {
            CFPreferencesSetAppValue(CFSTR("hide-mirror"), kCFBooleanTrue,CFSTR("com.apple.dock"));
            goto out;
        }
    }
    CFPreferencesSetAppValue(CFSTR("hide-mirror"), NULL,CFSTR("com.apple.dock"));
out:
    CFPreferencesAppSynchronize(CFSTR("com.apple.dock"));
    CFRelease(kext_infos);
    const char *dock = "/System/Library/CoreServices/Dock.app/Contents/MacOS/Dock";
    execl(dock,dock,NULL);
    return 1;
}
